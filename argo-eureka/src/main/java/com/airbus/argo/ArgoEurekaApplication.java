package com.airbus.argo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ArgoEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArgoEurekaApplication.class, args);
	}

}
