package com.airbus.argo.security;

public enum UserRole {
	USER, ADMIN;
}
