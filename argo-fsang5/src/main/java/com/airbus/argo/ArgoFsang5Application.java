package com.airbus.argo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@EnableEurekaClient
@SpringBootApplication
public class ArgoFsang5Application {

	public static void main(String[] args) {
		SpringApplication.run(ArgoFsang5Application.class, args);
	}

}
