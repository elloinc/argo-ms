package com.airbus.argo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@EnableEurekaClient
@SpringBootApplication
public class ArgoAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArgoAuthApplication.class, args);
	}

}
