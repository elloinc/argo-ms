package com.airbus.argo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@EnableEurekaClient
@SpringBootApplication
public class ArgoIwacsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArgoIwacsApplication.class, args);
	}

}
